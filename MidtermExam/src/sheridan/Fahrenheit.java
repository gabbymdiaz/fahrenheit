package sheridan;

public class Fahrenheit {

	public static void main(String[] args) {
		System.out.println("Result = " + convertToCelsius(75));

	}
	
	public static int convertToCelsius(int temperature) {
		double fahrenheit = (double) temperature;
		double celsius = ((fahrenheit - 32) * 5) / 9;
		
		int celsiusRounded = (int) Math.ceil(celsius);
		
		return celsiusRounded;
	}

}
