package sheridan;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class FahrenheitTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	// Regular/Happy path
	@Test
	public void ConvertToCelsiusTest() {
		int temp = Fahrenheit.convertToCelsius(75);
		assertTrue("Invalid input", temp == 24);
	}
	
	// Error/Exceptional path
	@Test(expected = Exception.class)
	public void ConvertToCelsiusTestException() {
		int temp = Fahrenheit.convertToCelsius(Integer.parseInt("hello"));
		assertFalse("Invalid input", temp != 24);
	}
	
	// Boundary in path
	@Test
	public void ConvertToCelsiusTestBoundaryIn() {
		int temp = Fahrenheit.convertToCelsius(75);
		assertTrue("Invalid input", temp > 23);
	}
	
	// Boundary out path
	@Test
	public void ConvertToCelsiusTestBoundaryOut() {
		int temp = Fahrenheit.convertToCelsius(75);
		assertFalse("Invalid input", temp < 24);
	}

}
